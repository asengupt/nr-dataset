# NonRigidDataset

This dataset contains two simulated data:

1) Board

2) Cube


The image and depth files are provided in the respective folders. The object models at the initial, undeformed condition are also provided (in .obj format).



------------------------

The camera parameters are as follows:


C_x  320.0

C_y  240.0

F_x  700.0

F_y  700.0

The depth and color camera are the same, unlike real depth sensors. Therefore, no transformation between camera and depth sensor.


------------------------

The ground-truth for the deformation are provided in a .tar file inside the respective folders.

------------------------


This dataset (in the 'master' branch) has been utilized in the paper: 


**Sengupta, Agniva, Alexandre Krupa, and Eric Marchand. "Tracking of Non-Rigid Objects using RGB-D Camera." IEEE International Conference on Systems, Man, and Cybernetics, SMC 2019. Bari, Italy.**